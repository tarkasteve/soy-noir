(ns soy-noir.views.welcome
  (:require [clojure.java.io :as io])
  (:use [noir.core :only [defpage]])
  (:import (java.io File)
           (com.google.template.soy SoyFileSet SoyFileSet$Builder)
           (com.google.template.soy.data SoyMapData)
           (com.google.template.soy.tofu SoyTofu)))

(defpage "/soy" []
  (let [tmpl (io/resource  "templates/welcome.soy")
        renderer (-> (SoyFileSet$Builder.)
                     (.add tmpl)
                     (.build)
                     (.compileToTofu)
                     (.newRenderer "welcome.greeting"))]
    (-> (.setData renderer {"name" "Steve"})
        (.render))))
