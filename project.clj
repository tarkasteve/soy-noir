(defproject soy-noir "0.1.0-SNAPSHOT"
  :description "FIXME: write this!"

  :repositories {"local" ~(-> (java.io.File. "mvn_local") (.toURI) (str))}
  :dependencies [[org.clojure/clojure "1.4.0"]
                 [noir "1.3.0-beta3"]
                 [com.google.template/soy "2011-22-12"]]

  :main soy-noir.server)

