# soy-noir

Example of using Soy as a renderer for the Noir Clojure framework.

## Usage

If you use cake, substitute 'lein' with 'cake' below. Everything should work fine.

```bash
lein deps
lein run
```

## License

Copyright (C) 2012

Distributed under the Eclipse Public License, the same as Clojure.

